

def calcular_ingresos(datos: list) -> dict:
    # Tipos de cilindrajes
    bajo_cilindraje: str = "bajo_cilindraje"
    alto_cilindraje: str = "alto_cilindraje"
    # Variable para almacenar el total de todos los cilindrajes
    total: int = 0
    # Variable/contador de bajo cilindraje
    cant_bajo_cilindraje: int = 0
    # Variable/contador de alto cilindraje
    cant_alto_cilindraje: int = 0
    # Variable que representa los ingresos totales de bajo cilindraje
    total_bajo_cilindraje: int = 0
    # Variable que representa los ingresos totales de alto cilindraje
    total_alto_cilindraje: int = 0
    # Itera los datos a procesar
    for item in datos:
        # Suma el total de ingresos
        total += item["valor_pagar"]
        # Obtiene el total de los seguros
        if item["tipo_seguro"] == bajo_cilindraje:
            cant_bajo_cilindraje += 1
            total_bajo_cilindraje += item["valor_pagar"]
        elif item["tipo_seguro"] == alto_cilindraje:
            cant_alto_cilindraje += 1
            total_alto_cilindraje += item["valor_pagar"]

    promedio_bajo_cilindraje: float = 0
    promedio_alto_cilindraje: float = 0
    # Valida para que no se dé un caso de 0/0
    if cant_bajo_cilindraje > 0:
        promedio_bajo_cilindraje = round((total_bajo_cilindraje / cant_bajo_cilindraje), 1)
    if cant_alto_cilindraje > 0:
        promedio_alto_cilindraje = round((total_alto_cilindraje / cant_alto_cilindraje), 1)
    # Debe retornar
    resp: dict = {
        "total": total,
        "promedio_bajo_cilindraje": promedio_bajo_cilindraje,
        "promedio_alto_cilindraje": promedio_alto_cilindraje
    }

    return resp


# caso 1
datos: list = [
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 100000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 50000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 250000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 480000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 20000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 40000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 150000
    }
]
print(calcular_ingresos(datos))

# caso 2
datos: list = [
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 300000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 460000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 105000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 90000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 50000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 95000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 68000
    }
]
print(calcular_ingresos(datos))

# Caso 3
datos: list = [
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 600000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 85000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 46000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 46000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 305000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 78000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 199000
    }
]
print(calcular_ingresos(datos))

# Caso 4
datos: list = [
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 28000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 25000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 10000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 80000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 8000
    },
    {
        "tipo_seguro": "alto_cilindraje",
        "valor_pagar": 5000
    },
    {
        "tipo_seguro": "bajo_cilindraje",
        "valor_pagar": 2000
    }
]
print(calcular_ingresos(datos))

