def cambio_dogecoin (informacion: dict) -> dict:

    # Uso Variables
    b1 = informacion['dgc']
    b2 = informacion['usuario']
    b3 = informacion['billetera']
    b4 = informacion['cambio']
    b5 = informacion['saldo_inicial']
    b6 = informacion['nuevo_saldo']


    # Arbol de decisiones
    if b3 == "3er45ty67u":
        if b4 > 1000000:
            b4 = b4 + 50000
            if b4 <= b5:
                b6 = (b5 - b4)
                mensaje = "Su billetera tiene actualmente: {}".format(b6)
            else:
                mensaje = "El cambio a Dogecoin supera el saldo"             
        else: 
            if b4 <= b5:
                b6 = (b5 - b4)
                mensaje = "Su billetera tiene actualmente: {}".format(b6)
            else:
                mensaje = "El cambio a Dogecoin supera el saldo"
    else:            
        mensaje = "Su billetera no existe"
    
    diccionario_respuesta = {
        'dgc': b1,
        'usuario': b2,
        'mensaje': mensaje
    }

    return diccionario_respuesta

