def promedio_salarial(Laura: int, Juan: int, Andres: int, Sara: int, Natalia: int) -> str: 
    #Convertir los salarios de las mujeres a pesos
    Laura *= 3600
    Sara *= 3600
    Natalia *= 3600
    promedio_salarios = (Laura + Sara + Natalia + Andres + Juan) / 5

    return f"El promedio salarial es: {round(promedio_salarios, 2)} pesos colombianos"


print(promedio_salarial(1050, 1800000, 2750000, 2100, 3500))